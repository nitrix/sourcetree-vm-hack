#!/usr/bin/env php
<?php
$cd = $argv[1];
$arg = $argv[2];

$arg = str_replace('_C', '\'', $arg);
$arg = str_replace('_B', '"', $arg);
$arg = str_replace('_A', '_', $arg);
$arg = str_replace("\n", '', $arg);

if (substr($cd, 0, 3) == '%LETTER%:\\') {
	$cd = '%PATH%/'.str_replace('\\','/',substr($cd, 3));
	$arg = str_replace('\\','/', str_replace('%LETTER%:\\', '%PATH%/', $arg));
}
chdir($cd);

exec('git '.$arg, $output, $ret);
echo implode("\n", $output);
exit($ret);
?>