import System.Process
import System.Directory
import System.Environment
import Data.String.Utils
import System.Environment.FindBin
import System.IO
import System.Exit
import Data.List
import System.FilePath
import Data.Ini
import Data.Either
import qualified Data.Text as T

main = do
	cd <- (\x -> T.unpack $ T.replace (T.pack "\\") (T.pack "\\\\") (T.pack x)) <$> getCurrentDirectory
	path <- getProgPath
	args <- getArgs
	conf <- readIniFile $ path ++ "\\git.conf"
	
	let c_ip = either (const "") T.unpack $ lookupValue (T.pack "REMOTE") (T.pack "ip") (head $ rights [conf])
	let c_path = either (const "") T.unpack $ lookupValue (T.pack "REMOTE") (T.pack "path") (head $ rights [conf])
	let c_username = either (const "") T.unpack $ lookupValue (T.pack "REMOTE") (T.pack "username") (head $ rights [conf])
	let c_password = either (const "") T.unpack $ lookupValue (T.pack "REMOTE") (T.pack "password") (head $ rights [conf])
	let c_letter = either (const "") T.unpack $ lookupValue (T.pack "MOUNT") (T.pack "letter") (head $ rights [conf])
	
	if head cd == (head c_letter) then do
		createDirectoryIfMissing False (c_letter ++ ":\\.tmp")
		if (elem "mergetool" args || elem "difftool" args) then do
			(code, out, err) <- readProcessWithExitCode (path ++ "\\..\\bak\\git.exe") args ""
			final code out err
		else do
			some <- mapM argReplace (zip3 (repeat c_letter) (repeat c_path) args)
			let argv = unwords some
			let finalArgv = [c_username ++ "@" ++ c_ip, "-pw", c_password, "'" ++ c_path ++ "/.git.php' '" ++ cd ++ "' '" ++ argv ++ "'"]
			(code, out, err) <- readProcessWithExitCode (path ++ "\\plink.exe") finalArgv ""
			final code out err
	else do
		(code, out, err) <- readProcessWithExitCode (path ++ "\\..\\bak\\git.exe") args ""
		final code out err
	
final code out err = do
	hPutStrLn stdout out
	hPutStrLn stderr err
	exitWith code

argReplace :: (String, String, String) -> IO String
argReplace (c_letter, c_path, str) = do
	let out = "_C" ++ (replace "'" "_C" $ replace "\"" "_B" $ replace "_" "_A" str) ++ "_C"
	
	case isPrefixOf "C:\\" str of
		True -> do 
					let (dir, file) = splitFileName str
					copyFile str $ c_letter ++ ":\\.tmp\\" ++ file
					return $ c_path ++ "/.tmp/" ++ file
		False -> return out