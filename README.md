Requirements for this hack:

- The address of the server running for which a network drive is mapped to a letter on your machine (for initial setup & commits).
- SSH access to that server (either using Pageant, LDAP or a user/password combination).
- SourceTree configured to use its own embedded Git (to provide a safe sandbox around my hack).
- Because the server will now do push and pulls, it'll need access to the git repos you're using.

That's it.
